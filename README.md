1. Task#1: Write a rule that will reverse the elements of a list.
2. Task#2: Write a rule that will ﬁnd the smallest element of a list.
3. Task#3: Consider the following knowledge base: 
nonStopFlight(pittsburgh, cleveland).
nonStopFlight(philadelphia, pittsburgh). 
nonStopFlight(columbus, philadelphia). 
nonStopFlight(sanFrancisco, columbus). 
nonStopFlight(detroit, sanFrancisco). 
nonStopFlight(toledo, detroit). 
nonStopFlight(houston, toledo).

Write a recursive predicate findFlight/2 that tells us when we can travel by plane between two towns.


Instructions for compiling and running this Prolog file

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:


1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_PROLOGII
. Type this command to log in to SWI-Prolog : swipl
. Test if the erl file has no error existing: ['FILENAME']. (Ex: ['index'].)
. Compile and run each function by command: FUNCTION_NAME(arg). (Ex: function1(X). then type ; to see next possible results.)
-> the solutions for all functions will show up as the example below

2. Using PowerShell in Windows system
. Log in to the directory by command that contains files: cd /d YOURDIRECTORY (Ex: cd \d C:\Windows)
. Type this command to log in to SWI-Prolog: swipl
. Test if the erl file has no error existing: ['FILENAME']. (Ex: ['index'].)
. Compile and run each function by command: FUNCTION_NAME(arg,arg). (Ex: function1(X,[1,2,a,b]). then type ; to see next possible results.)
-> the solutions for all functions will show up as the example below



                            Example Solutions for all functions
                            
//                          OPENING WINDOWS POWERSHELL  
// =============================================================================
Windows PowerShell
Copyright (C) 2014 Microsoft Corporation. All rights reserved.

PS C:\Users\ADMIN> cd .\PHUONG_HAI_NGUYEN_PROLOGII
PS C:\Users\ADMIN\PHUONG_HAI_NGUYEN_PROLOGII> swipl
Welcome to SWI-Prolog (threaded, 64 bits, version 7.6.0)
SWI-Prolog comes with ABSOLUTELY NO WARRANTY. This is free software.
Please run ?- license. for legal details.

For online help and background, visit http://www.swi-prolog.org
For built-in help, use ?- help(Topic). or ?- apropos(Word).

%%          Open and compile the file
%%===============================================
1 ?- ['index'].
true.

%%               Task #1 Solution
%%===============================================
2 ?- reverseList(ReverseOrder, [1,2,a,b,3,c,4,d]).
ReverseOrder = [d, 4, c, 3, b, a, 2, 1] .

%%               Task #2 Solution
%%===============================================
3 ?- smallest(SmallestVal, [29,30,7,12,1994,34,15,75,76,77,88,44,99]).
SmallestVal = 7 .

%%               Task #3 Solution
%%===============================================
4 ?- findFlight(X,Y).
X = pittsburgh,
Y = cleveland ;
X = philadelphia,
Y = pittsburgh ;
X = columbus,
Y = philadelphia ;
X = sanFrancisco,
Y = columbus ;
X = detroit,
Y = sanFrancisco ;
X = toledo,
Y = detroit ;
X = houston,
Y = toledo ;
X = philadelphia,
Y = cleveland ;
X = columbus,
Y = pittsburgh ;
X = columbus,
Y = cleveland ;
X = sanFrancisco,
Y = philadelphia ;
X = sanFrancisco,
Y = pittsburgh ;
X = sanFrancisco,
Y = cleveland ;
X = detroit,
Y = columbus ;
X = detroit,
Y = philadelphia ;
X = detroit,
Y = pittsburgh ;
X = detroit,
Y = cleveland ;
X = toledo,
Y = sanFrancisco ;
X = toledo,
Y = columbus ;
X = toledo,
Y = philadelphia ;
X = toledo,
Y = pittsburgh ;
X = toledo,
Y = cleveland ;
X = houston,
Y = detroit ;
X = houston,
Y = sanFrancisco ;
X = houston,
Y = columbus ;
X = houston,
Y = philadelphia ;
X = houston,
Y = pittsburgh ;
X = houston,
Y = cleveland.

6 ?- findFlight(cleveland, detroit).
false.

7 ?- findFlight(detroit, columbus).
true.