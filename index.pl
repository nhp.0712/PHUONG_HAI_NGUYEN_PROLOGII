%% Task#1: Write a rule that will reverse the elements of a list.
%%===========================================================================================
reverseList([],[]).
reverseList([Head|Tail], Result) :- reverseList(Tail, What), append(What, [Head], Result).

%% Task#2: Write a rule that will ﬁnd the smallest element of a list.
%%===========================================================================================
min(A, A, B) :- (A =< B).
min(B, A, B) :- (B =< A).
smallest(A, [A|[]]).
smallest(What, [A|B]) :- smallest(Result, B), min(What, A, Result).

%% Task#3: Consider the following knowledge base: 
%%      nonStopFlight(pittsburgh, cleveland).
%%      nonStopFlight(philadelphia, pittsburgh). 
%%      nonStopFlight(columbus, philadelphia). 
%%      nonStopFlight(sanFrancisco, columbus). 
%%      nonStopFlight(detroit, sanFrancisco). 
%%      nonStopFlight(toledo, detroit). 
%%      nonStopFlight(houston, toledo).

%%  Write a recursive predicate findFlight/2 that tells us when we can travel by plane between two towns.
%%===========================================================================================
nonStopFlight(pittsburgh, cleveland). 
nonStopFlight(philadelphia, pittsburgh). 
nonStopFlight(columbus, philadelphia). 
nonStopFlight(sanFrancisco, columbus). 
nonStopFlight(detroit, sanFrancisco). 
nonStopFlight(toledo, detroit). 
nonStopFlight(houston, toledo).

findFlight(X, Y) :- nonStopFlight(X, Y).
findFlight(X ,Y) :- nonStopFlight(X, Z), findFlight(Z, Y).